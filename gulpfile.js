'use strict';
const gulp   = require('gulp'),
    gutil  = require('gulp-util'),
    argv   = require('minimist')(process.argv),
    gulpif = require('gulp-if'),
    prompt = require('gulp-prompt'),
    rsync  = require('gulp-rsync'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-clean-css'),
    include = require("gulp-include"),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    runSequence = require('run-sequence'),
    htmlmin = require('gulp-htmlmin'),
    server = require('gulp-express');

const path = {
    build: {
        html: 'public/',
        components_dev: 'public/components/',
        css: 'public/stylesheets/',
        img: 'public/images/',
        fonts: 'public/fonts/'
    },
    src: {
        html: ['front/**/*.html','!front/templates/**/_*.html','!front/components/**/*.*'],
        components_dev: 'front/components/**/*.*',
        style: 'front/stylesheets/main.scss',
        img: 'front/images/**/*',
        fonts: ['front/fonts/**/*.*']
    },
    watch:{
        html: 'front/**/*.html',
        style: 'front/stylesheets/**/*.scss',
        img: 'front/images/**/*.*',
        fonts: 'front/fonts/**/*.*',
        server: ['bin/www','app.js','./**/*.js', '!front/**/*.js', '!public/**/*.js']
    }
};
let options = {
    cwd: undefined
};
options.env = process.env;
options.env.NODE_ENV = 'development';

gulp.task('server', function () {
    server.stop();
    server.run(['bin/www'],options,false);
});

gulp.task('sass', () => {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(gulp.dest(path.build.css))
});

gulp.task('fonts',() => {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('image',() => {
    return gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.build.img));
});

gulp.task('html',() => {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(include())//Объединим с шаблонами
        .pipe(htmlmin({collapseWhitespace: true}))//минимизируем
        .pipe(gulp.dest(path.build.html)); //Выплюнем их в папку build
});

gulp.task('watch', () => {
    watch([path.watch.html], () => {
        gulp.start('html');
    });
    watch([path.watch.style], () => {
        gulp.start('sass');
    });
    watch([path.watch.img], () => {
        gulp.start('image');
    });
    watch([path.watch.fonts], () => {
        gulp.start('fonts');
    });
    watch(path.watch.server, () => {
        gulp.start('server');
    });
});

gulp.task('start', cb => runSequence(['universal'], cb));

gulp.task('universal', cb => runSequence(['fonts', 'sass', 'image', 'html'], cb));

gulp.task('default', cb =>  runSequence(['universal','server','watch'], cb));

gulp.task('deploy', () => {
    const rsyncPaths = ['public', 'package.json', 'bin', 'routes', 'app.js', 'models'];
    const  rsyncConf = {
        incremental: true,
        relative: true,
        emptyDirectories: true,
        recursive: true,
        clean: true,
        links: true,
        command: true,
        exclude: ['public/images/upload']
    };
    // Staging
    if (argv.staging) {
        rsyncConf.hostname = '212.233.113.230'; // hostname
        rsyncConf.port = 2222; // port
        rsyncConf.username = 'dev'; // ssh username
        rsyncConf.destination = '/home/dev/test'; // path where uploaded files go
        // Production
    } else if (argv.production) {

        rsyncConf.hostname = process.env.PRO_HOST || ''; // hostname
        rsyncConf.username = process.env.PRO_USER || ''; // ssh username
        rsyncConf.port = +process.env.PRO_PORT || 22; // port
        rsyncConf.destination = process.env.PRO_PATH || ''; // path where uploaded files go
        // Missing/Invalid Target
    } else {
        throwError('deploy', gutil.colors.red('Missing or invalid target'));
    }
    return gulp.src(rsyncPaths)
    // .pipe(gulpif(
    //     argv.production,
    //     prompt.confirm({
    //         message: 'Heads Up! Are you SURE you want to push to PRODUCTION?',
    //         default: false
    //     })
    // ))
        .pipe(rsync(rsyncConf));
});

function throwError(taskName, msg) {
    throw new gutil.PluginError({
        plugin: taskName,
        message: msg
    });
}